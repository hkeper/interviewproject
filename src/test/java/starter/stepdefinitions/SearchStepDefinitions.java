package starter.stepdefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.module.jsv.JsonSchemaValidator;
import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Steps;
import starter.model.Provider;
import java.util.List;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertTrue;

public class SearchStepDefinitions {

    @Steps
    public CarsAPI carsAPI;

    //Unmodified steps
    @When("he calls endpoint {string}")
    public void heCallsEndpoint(String arg0) {
        SerenityRest.given().get(arg0);
    }

    @Then("he sees the results displayed for apple")
    public void heSeesTheResultsDisplayedForApple() {
        restAssuredThat(response -> response.statusCode(200));
    }

    @Then("he sees the results displayed for mango")
    public void heSeesTheResultsDisplayedForMango() {
        restAssuredThat(response -> response.body("title", contains("mango")));
    }

    @Then("he doesn not see the results")
    public void he_Doesn_Not_See_The_Results() {
        restAssuredThat(response -> response.body("error", contains("True")));
    }

    //Added/Modified steps
    @When("User ask for product {word}")
    public void userCallsEndpoint(String product) {
        carsAPI.fetchProvidersByProduct(product);
    }

    @Then("User sees list with (.*) - (.*)$")
    public void userSeesTheResultsDisplayedForApple(String parameter,String value) {
        Response response = SerenityRest.lastResponse();

        /*
        Check JSON product response according to schema 'src/test/resources/provider_schema.json'
        In JSON schema(provider_schema.json) of product response was deleted field "promoDetails",
         because it has different types string/boolean, it can be a bug, investigation in progress.
         */
        restAssuredThat(restResponse ->
                restResponse.statusCode(200)
                        .body(JsonSchemaValidator.matchesJsonSchemaInClasspath("provider_schema.json"))
        );

        //Verify that list of parameters has value
        List<String> parameters = response.path(parameter);
        assertThat(parameters, hasItems(value));
    }
    @Then("User sees the error in result")
    public void userDoesNotSeeTheResults() {
        restAssuredThat(response -> response
                .statusCode(404)
                .body("detail.error", equalTo(true))
        );
    }

    @Then("User sees no zero or negative prices")
    public void checkForZeroNegativePrices() {
        //Get POJO and verify no below 0 prices
        List<Provider> providers = SerenityRest.lastResponse()
                .jsonPath()
                .getList("", Provider.class);

        providers.forEach((provider) -> assertTrue(provider.getPrice() > 0));
    }

}
