package starter.stepdefinitions;

import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;
public class CarsAPI {
    private static String PROVIDER_BY_PRODUCT = "https://waarkoop-server.herokuapp.com/api/v1/search/test/{product}";

    @Step("User calls endpoint {0}")
    public void fetchProvidersByProduct(String product) {
        SerenityRest.given()
                .pathParam("product", product)
                .get(PROVIDER_BY_PRODUCT);
    }

}
