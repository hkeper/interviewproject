Feature: Search for the available products

  Scenario Outline:
    When User ask for product <Product>
    Then User sees list with "provider" - <Provider>
    And User sees list with "title" - <Title>
    And User sees list with "brand" - <Brand>
    And User sees no zero or negative prices
    Examples:
      | Product | Provider | Title                                    | Brand         |
      | apple   | Vomar    | Rivella Pineapple                        | Crystal Clear |
      | mango   | Coop     | Fuze Tea Green Tea Mango Chamomile 1,5 L | Pickwick      |
