package starter.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Provider {
    @JsonProperty("provider")
    private String provider;
    @JsonProperty("title")
    private String title;
    @JsonProperty("brand")
    private String brand;
    @JsonProperty("price")
    private Float price;

    public Provider(){}
    public Provider(String provider, String title, String brand, Float price) {
        this.provider = provider;
        this.title = title;
        this.brand = brand;
        this.price = price;
    }
    public String getProvider() {
        return provider;
    }
    public String getTitle() {
        return title;
    }
    public String getBrand() {
        return brand;
    }
    public Float getPrice() {
        return price;
    }

}
