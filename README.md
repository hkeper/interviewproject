## Automated API tests using Serenity, Cucumber and Maven

The best place to start to clone or download tests from GitLab ([https://gitlab.com/hkeper/interviewproject.git](https://gitlab.com/hkeper/interviewproject.git)). 

The project contains API tests for search products. You can find Cucumber scenarios in the directory _resources/features_.

Run the tests like this:

```
mvn clean verify
```

By default, the tests run only features running, for product search. You can change the which features should be running to modify file `TestRunner` to add feature files to option **features** .

## How to add tests

This project uses Serenity BDD framework to create tests, for more information see official documentation - [https://serenity-bdd.github.io/theserenitybook/latest/index.html](https://serenity-bdd.github.io/theserenitybook/latest/index.html)


### !What was refactored

I decided to add scenarios for negative and positive in separate files in folder _resources/features/search/refactored_.

Add to CarsAPI implementation to have URL in one place and add method to open url with parameter search product.

Add model file `Provider` for product response JSON with provider information. 


